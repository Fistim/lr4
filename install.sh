ARCHIVE=$PWD
INSTDIR=/home/$USER/install_vm_lr4_eosi
cd $HOME
mkdir $INSTDIR
cd $INSTDIR
#Download VMs
# wget -q --show-progress https://cloud.tomtit-tomsk.ru/index.php/s/Rtsbr96yDnHEAaz/download/PracticalVM.tar.gz

#Unpack VM from archives
echo $(date +%T) "Unpacking virtual machines..."
tar -xvf $ARCHIVE/PracticalVM.tar.gz &> /dev/null


#Export VMs to VMWare Workstation Pro
echo $(date +%T) "Exporting TCPDUMP VM..."
ovftool -tt=vmx $INSTDIR/TCPDUMP.ovf $HOME/vmware &> /dev/null
echo $(date +%T) "Exporting CLI VM..."
ovftool -tt=vmx $INSTDIR/CLI.ovf $HOME/vmware &> /dev/null

# Remove files
cd $HOME
rm -r $INSTDIR

#Register & Start VMs
echo $(date +%T) "Registering and starting VMs"
echo $(date +%T) ""
echo $(date +%T) "Credentials for both of VMs:"
echo $(date +%T) "Username: locadm"
echo $(date +%T) "Password: QWEasd123"
vmrun start $HOME/vmware/CLI/CLI.vmx
vmrun start $HOME/vmware/TCPDUMP/TCPDUMP.vmx
